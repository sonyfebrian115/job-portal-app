-- MariaDB dump 10.19  Distrib 10.5.19-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: job_app
-- ------------------------------------------------------
-- Server version	10.5.19-MariaDB-0+deb11u2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `job_app`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `job_app` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;

USE `job_app`;

--
-- Table structure for table `job_details`
--

DROP TABLE IF EXISTS `job_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `responsibilities` text DEFAULT NULL,
  `qualifications` text DEFAULT NULL,
  `benefits` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `minimum_experience` varchar(50) DEFAULT NULL,
  `skills` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `job_id` (`job_id`),
  CONSTRAINT `job_details_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_details`
--

LOCK TABLES `job_details` WRITE;
/*!40000 ALTER TABLE `job_details` DISABLE KEYS */;
INSERT INTO `job_details` VALUES (1,1,'Design and develop software applications.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis faucibus ipsum eget iaculis aliquam. Nam et ex leo. Maecenas ut vehicula nunc. Proin et gravida odio. Suspendisse in suscipit arcu. Ut quam massa, aliquet ullamcorper tortor in, egestas auctor quam. In ultrices metus dolor, sit amet consequat felis mollis quis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras nisi mi, pharetra ornare diam eget, tempus interdum purus. Sed interdum pretium felis quis consectetur. Pellentesque et augue lorem. Nullam id facilisis sem. Sed id nulla vitae ante lacinia porta a efficitur libero. Fusce sodales nec purus eget tincidunt.\n\nMorbi dictum nunc sed nulla rutrum scelerisque. Pellentesque dictum nec justo in scelerisque. Nunc ut nulla nibh. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut pharetra, augue placerat luctus blandit, orci nisi cursus leo, ac efficitur sem nulla et sem. Donec maximus consequat libero vel dictum. Aenean eu mi vel lectus consequat sodales nec eget dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Praesent laoreet, est non ullamcorper laoreet, quam eros pulvinar mauris, a elementum urna est tempus neque. Vestibulum molestie ut odio sed congue. Etiam vulputate ex eu enim varius, quis maximus orci molestie.','Bachelor degree in Computer Science...','Health insurance, paid vacation...','2023-07-21 04:45:41','2023-07-22 10:30:09',NULL,NULL),(2,2,'Plan and execute marketing strategies. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis faucibus ipsum eget iaculis aliquam. Nam et ex leo. Maecenas ut vehicula nunc. Proin et gravida odio. Suspendisse in suscipit arcu. Ut quam massa, aliquet ullamcorper tortor in, egestas auctor quam. In ultrices metus dolor, sit amet consequat felis mollis quis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras nisi mi, pharetra ornare diam eget, tempus interdum purus. Sed interdum pretium felis quis consectetur. Pellentesque et augue lorem. Nullam id facilisis sem. Sed id nulla vitae ante lacinia porta a efficitur libero. Fusce sodales nec purus eget tincidunt.\n\nMorbi dictum nunc sed nulla rutrum scelerisque. Pellentesque dictum nec justo in scelerisque. Nunc ut nulla nibh. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut pharetra, augue placerat luctus blandit, orci nisi cursus leo, ac efficitur sem nulla et sem. Donec maximus consequat libero vel dictum. Aenean eu mi vel lectus consequat sodales nec eget dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Praesent laoreet, est non ullamcorper laoreet, quam eros pulvinar mauris, a elementum urna est tempus neque. Vestibulum molestie ut odio sed congue. Etiam vulputate ex eu enim varius, quis maximus orci molestie.','Bachelor degree in Marketing...','Bonus based on performance...','2023-07-21 04:45:53','2023-07-22 10:30:10',NULL,NULL),(3,3,'Design and develop software applications. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis faucibus ipsum eget iaculis aliquam. Nam et ex leo. Maecenas ut vehicula nunc. Proin et gravida odio. Suspendisse in suscipit arcu. Ut quam massa, aliquet ullamcorper tortor in, egestas auctor quam. In ultrices metus dolor, sit amet consequat felis mollis quis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras nisi mi, pharetra ornare diam eget, tempus interdum purus. Sed interdum pretium felis quis consectetur. Pellentesque et augue lorem. Nullam id facilisis sem. Sed id nulla vitae ante lacinia porta a efficitur libero. Fusce sodales nec purus eget tincidunt.\n\nMorbi dictum nunc sed nulla rutrum scelerisque. Pellentesque dictum nec justo in scelerisque. Nunc ut nulla nibh. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut pharetra, augue placerat luctus blandit, orci nisi cursus leo, ac efficitur sem nulla et sem. Donec maximus consequat libero vel dictum. Aenean eu mi vel lectus consequat sodales nec eget dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Praesent laoreet, est non ullamcorper laoreet, quam eros pulvinar mauris, a elementum urna est tempus neque. Vestibulum molestie ut odio sed congue. Etiam vulputate ex eu enim varius, quis maximus orci molestie.','Bachelor degree in Computer Science...','Health insurance, paid vacation...','2023-07-21 10:13:47','2023-07-22 10:30:10','2 years','JavaScript, React, Node.js'),(4,4,'Plan and execute marketing strategies. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis faucibus ipsum eget iaculis aliquam. Nam et ex leo. Maecenas ut vehicula nunc. Proin et gravida odio. Suspendisse in suscipit arcu. Ut quam massa, aliquet ullamcorper tortor in, egestas auctor quam. In ultrices metus dolor, sit amet consequat felis mollis quis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras nisi mi, pharetra ornare diam eget, tempus interdum purus. Sed interdum pretium felis quis consectetur. Pellentesque et augue lorem. Nullam id facilisis sem. Sed id nulla vitae ante lacinia porta a efficitur libero. Fusce sodales nec purus eget tincidunt.\n\nMorbi dictum nunc sed nulla rutrum scelerisque. Pellentesque dictum nec justo in scelerisque. Nunc ut nulla nibh. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut pharetra, augue placerat luctus blandit, orci nisi cursus leo, ac efficitur sem nulla et sem. Donec maximus consequat libero vel dictum. Aenean eu mi vel lectus consequat sodales nec eget dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Praesent laoreet, est non ullamcorper laoreet, quam eros pulvinar mauris, a elementum urna est tempus neque. Vestibulum molestie ut odio sed congue. Etiam vulputate ex eu enim varius, quis maximus orci molestie.','Bachelor degree in Marketing...','Bonus based on performance...','2023-07-21 10:14:04','2023-07-22 10:30:10','1 year','Digital marketing, SEO, SEM'),(5,5,'Design and develop software applications. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis faucibus ipsum eget iaculis aliquam. Nam et ex leo. Maecenas ut vehicula nunc. Proin et gravida odio. Suspendisse in suscipit arcu. Ut quam massa, aliquet ullamcorper tortor in, egestas auctor quam. In ultrices metus dolor, sit amet consequat felis mollis quis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras nisi mi, pharetra ornare diam eget, tempus interdum purus. Sed interdum pretium felis quis consectetur. Pellentesque et augue lorem. Nullam id facilisis sem. Sed id nulla vitae ante lacinia porta a efficitur libero. Fusce sodales nec purus eget tincidunt.\n\nMorbi dictum nunc sed nulla rutrum scelerisque. Pellentesque dictum nec justo in scelerisque. Nunc ut nulla nibh. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut pharetra, augue placerat luctus blandit, orci nisi cursus leo, ac efficitur sem nulla et sem. Donec maximus consequat libero vel dictum. Aenean eu mi vel lectus consequat sodales nec eget dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Praesent laoreet, est non ullamcorper laoreet, quam eros pulvinar mauris, a elementum urna est tempus neque. Vestibulum molestie ut odio sed congue. Etiam vulputate ex eu enim varius, quis maximus orci molestie.','Bachelor degree in Computer Science...','Health insurance, paid vacation...','2023-07-21 10:25:43','2023-07-22 10:30:10','2 years','JavaScript, React, Node.js'),(6,6,'Design and develop software applications.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis faucibus ipsum eget iaculis aliquam. Nam et ex leo. Maecenas ut vehicula nunc. Proin et gravida odio. Suspendisse in suscipit arcu. Ut quam massa, aliquet ullamcorper tortor in, egestas auctor quam. In ultrices metus dolor, sit amet consequat felis mollis quis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras nisi mi, pharetra ornare diam eget, tempus interdum purus. Sed interdum pretium felis quis consectetur. Pellentesque et augue lorem. Nullam id facilisis sem. Sed id nulla vitae ante lacinia porta a efficitur libero. Fusce sodales nec purus eget tincidunt.\n\nMorbi dictum nunc sed nulla rutrum scelerisque. Pellentesque dictum nec justo in scelerisque. Nunc ut nulla nibh. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut pharetra, augue placerat luctus blandit, orci nisi cursus leo, ac efficitur sem nulla et sem. Donec maximus consequat libero vel dictum. Aenean eu mi vel lectus consequat sodales nec eget dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Praesent laoreet, est non ullamcorper laoreet, quam eros pulvinar mauris, a elementum urna est tempus neque. Vestibulum molestie ut odio sed congue. Etiam vulputate ex eu enim varius, quis maximus orci molestie.','Bachelor degree in Computer Science...','Health insurance, paid vacation...','2023-07-21 10:27:00','2023-07-22 10:30:10','2 years','JavaScript, React, Node.js'),(7,7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis faucibus ipsum eget iaculis aliquam. Nam et ex leo. Maecenas ut vehicula nunc. Proin et gravida odio. Suspendisse in suscipit arcu. Ut quam massa, aliquet ullamcorper tortor in, egestas auctor quam. In ultrices metus dolor, sit amet consequat felis mollis quis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras nisi mi, pharetra ornare diam eget, tempus interdum purus. Sed interdum pretium felis quis consectetur. Pellentesque et augue lorem. Nullam id facilisis sem. Sed id nulla vitae ante lacinia porta a efficitur libero. Fusce sodales nec purus eget tincidunt.','Bachelor degree in Computer Science...','Health insurance, paid vacation...','2023-07-22 11:07:04','2023-07-22 11:07:04','1 years','JavaScript, React, Node.js'),(8,8,'Maecenas ut vehicula nunc.','Bachelor degree in Computer Science...','Health insurance, paid vacation...','2023-07-22 11:28:46','2023-07-22 11:28:46','1 years','JavaScript, React, Node.js');
/*!40000 ALTER TABLE `job_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `employment_type` varchar(50) DEFAULT NULL,
  `position_level` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` VALUES (1,'Software Engineer','We are hiring a Software Engineer...','2023-07-21 04:45:25','2023-07-21 10:09:08','Fulltime','CEO/GM/Directur/Manajer Senior'),(2,'Marketing Specialist','We are looking for a Marketing Specialist...','2023-07-21 04:45:25','2023-07-21 10:08:14','Intership','Lulusan baru/ pengalaman kurang dari 1 tahun'),(3,'Frontend Developer','We are looking for a Frontend Developer...','2023-07-21 06:23:15','2023-07-21 10:09:08','Contract','Supervisor/koordinator'),(4,'Sales Executive','Join our sales team as a Sales Executive...','2023-07-21 06:23:28','2023-07-21 10:08:14','Part-time','Pegawai non-manajemegen & non-supervisor'),(5,'Sales Manager','Join our sales team as a Sales Manager...','2023-07-21 10:22:57','2023-07-21 10:22:57','Full-time','Manager'),(6,'Backend Developer','We are looking for a Backend Developer...','2023-07-21 10:23:12','2023-07-21 10:23:12','Full-time','Senior'),(7,'QA','We are looking for a QA Tester...','2023-07-22 11:05:21','2023-07-22 11:05:21','Contract','pegawai non-magement & non-supervisor'),(8,'CUSTOMER SERVICE ONLINE','We are looking for a QA CUSTOMER SERVICE ONLINE...','2023-07-22 11:24:31','2023-07-22 11:24:31','Contract','pegawai non-magement & non-supervisor');
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-22 20:08:37
