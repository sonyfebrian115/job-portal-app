const express = require("express");
const mysql = require("mysql2");
const bodyParser = require("body-parser");

const app = express();
const port = 3001;

const connection = mysql.createConnection({
  host: "localhost",
  user: "user",
  password: "password",
  database: "job_app",
});

connection.connect((err) => {
  if (err) {
    console.error("Error connecting to database: ", err);
    return;
  }
  console.log("Connected to database!");
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/jobs", (req, res) => {
  const query = "SELECT * FROM jobs";
  connection.query(query, (err, results) => {
    if (err) {
      console.error("Error executing query: ", err);
      res.status(500).json({ error: "Error executing query" });
      return;
    }
    res.status(200).json(results);
  });
});

app.get("/jobs/:id", (req, res) => {
  const jobId = req.params.id;
  const query =
    "SELECT * FROM jobs LEFT JOIN job_details ON jobs.id = job_details.job_id WHERE jobs.id = ?";
  connection.query(query, [jobId], (err, results) => {
    if (err) {
      console.error("Error executing query: ", err);
      res.status(500).json({ error: "Error executing query" });
      return;
    }
    if (results.length === 0) {
      res.status(404).json({ error: "Job not found" });
      return;
    }
    res.status(200).json(results[0]);
  });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
