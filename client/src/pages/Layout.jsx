import { useState } from "react";
import Card from "../components/Card";

const employmentTypes = [
  {
    id: "1",
    name: "Full-time",
  },
  {
    id: "2",
    name: "Part-time",
  },
  {
    id: "3",
    name: "Contract",
  },
  {
    id: "4",
    name: "Temporary",
  },
  {
    id: "5",
    name: "Internship",
  },
];

const positionLevels = [
  {
    id: "1",
    name: "CEO/GM/Directur/Manajer Senior",
  },
  {
    id: "2",
    name: "Lulusan baru/ pengalaman kurang dari 1 tahun",
  },
  {
    id: "3",
    name: "Supervisor/koordinator",
  },
  {
    id: "4",
    name: "Pegawai non-manajemegen & non-supervisor",
  },
  {
    id: "5",
    name: "Manager",
  },
  {
    id: "6",
    name: "Senior",
  },
];

const Layout = () => {
  const [isSidebarOpen, setSidebarOpen] = useState(false);

  const [selectedEmploymentTypes, setSelectedEmploymentTypes] = useState([]);
  const [selectedPositionLevels, setSelectedPositionLevels] = useState([]);

  const handleCheckboxChange = (id, category) => {
    if (category === "employment_type") {
      setSelectedEmploymentTypes((prevState) =>
        prevState.includes(id)
          ? prevState.filter((type) => type !== id)
          : [...prevState, id]
      );
    } else if (category === "position_level") {
      setSelectedPositionLevels((prevState) =>
        prevState.includes(id)
          ? prevState.filter((level) => level !== id)
          : [...prevState, id]
      );
    }
  };

  const handleSidebarToggle = () => {
    setSidebarOpen(!isSidebarOpen);
  };

  const handleSidebarClose = () => {
    setSidebarOpen(false);
  };

  return (
    <>
      <button
        onClick={handleSidebarToggle}
        aria-controls="default-sidebar"
        type="button"
        className="inline-flex items-center p-2 mt-2 ml-3 text-sm text-gray-500 rounded-lg sm:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
      >
        <span className="sr-only">Open sidebar</span>
        <svg
          className="w-6 h-6"
          aria-hidden="true"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            clipRule="evenodd"
            fillRule="evenodd"
            d="M2 4.75A.75.75 0 012.75 4h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 4.75zm0 10.5a.75.75 0 01.75-.75h7.5a.75.75 0 010 1.5h-7.5a.75.75 0 01-.75-.75zM2 10a.75.75 0 01.75-.75h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 10z"
          ></path>
        </svg>
      </button>

      {/* desktop view */}
      <aside
        id="default-sidebar"
        className="fixed top-0 left-0 z-40 w-64 h-screen transition-transform -translate-x-full sm:translate-x-0"
        aria-label="Sidebar"
      >
        <div className="h-full px-3 py-4 overflow-y-auto bg-gray-50 dark:bg-gray-800">
          <h1 className="ml-3 text-2xl text-blue-800 font-extrabold">
            <span className="text-orange-400">Join</span> Our Team
          </h1>

          <div className="max-w-lg mx-auto mt-10">
            <h1 className="lg:w-2/3 ml-3 leading-relaxed text-base">
              Employment type
            </h1>
            <fieldset className="mb-5">
              {employmentTypes.map((item, i) => (
                <div className="flex items-center items-start my-4" key={i}>
                  <input
                    id={`employment_type_${item.id}`}
                    type="checkbox"
                    className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-4 w-4 rounded"
                    checked={selectedEmploymentTypes.includes(item.id)}
                    onChange={() =>
                      handleCheckboxChange(item.id, "employment_type")
                    }
                  />
                  <label
                    htmlFor="checkbox-1"
                    className="text-sm ml-3 font-medium text-gray-900"
                  >
                    {item.name}
                  </label>
                </div>
              ))}
            </fieldset>
          </div>
          <div className="max-w-lg mx-auto mt-10">
            <h1 className="lg:w-2/3 ml-3 leading-relaxed text-base">
              Position Level
            </h1>
            <fieldset className="mb-5">
              {positionLevels.map((item, i) => (
                <div className="flex items-center items-start my-4" key={i}>
                  <input
                    id={`position_level_${item.id}`}
                    type="checkbox"
                    className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-4 w-4 rounded"
                    checked={selectedPositionLevels.includes(item.id)}
                    onChange={() =>
                      handleCheckboxChange(item.id, "position_level")
                    }
                  />
                  <label
                    htmlFor="checkbox-1"
                    className="text-sm ml-3 font-medium text-gray-900"
                  >
                    {item.name}
                  </label>
                </div>
              ))}
            </fieldset>
          </div>
        </div>
      </aside>

      {/* mobile view */}
      <aside
        id="default-sidebar"
        className={`fixed top-0 left-0 z-40 w-64 h-screen transition-transform ${
          isSidebarOpen ? "translate-x-0" : "-translate-x-full"
        } sm:hidden bg-gray-50 dark:bg-gray-800`}
        aria-label="Sidebar"
      >
        <div className="h-full px-3 py-4 overflow-y-auto bg-gray-50 dark:bg-gray-800">
          <h1
            className="ml-3 text-2xl text-blue-800 font-extrabold"
            onClick={handleSidebarClose}
          >
            <span className="text-orange-400">Join</span> Our Team
          </h1>
          <div className="max-w-lg mx-auto mt-10">
            <h1 className="lg:w-2/3 ml-3 leading-relaxed text-base">
              Employment type
            </h1>
            <fieldset className="mb-5">
              {employmentTypes.map((item, i) => (
                <div className="flex items-center items-start my-4" key={i}>
                  <input
                    id={`employment_type_${item.id}`}
                    type="checkbox"
                    className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-4 w-4 rounded"
                    checked={selectedEmploymentTypes.includes(item.id)}
                    onChange={() =>
                      handleCheckboxChange(item.id, "employment_type")
                    }
                  />
                  <label
                    htmlFor="checkbox-1"
                    className="text-sm ml-3 font-medium text-gray-900"
                  >
                    {item.name}
                  </label>
                </div>
              ))}
            </fieldset>
          </div>
          <div className="max-w-lg mx-auto mt-10">
            <h1 className="lg:w-2/3 ml-3 leading-relaxed text-base">
              Position Level
            </h1>
            <fieldset className="mb-5">
              {positionLevels.map((item, i) => (
                <div className="flex items-center items-start my-4" key={i}>
                  <input
                    id={`position_level_${item.id}`}
                    type="checkbox"
                    className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-4 w-4 rounded"
                    checked={selectedPositionLevels.includes(item.id)}
                    onChange={() =>
                      handleCheckboxChange(item.id, "position_level")
                    }
                  />
                  <label
                    htmlFor="checkbox-1"
                    className="text-sm ml-3 font-medium text-gray-900"
                  >
                    {item.name}
                  </label>
                </div>
              ))}
            </fieldset>
          </div>
        </div>
      </aside>

      <div className="sm:ml-64 bg-gray-50 dark:bg-gray-800">
        <Card />
      </div>
    </>
  );
};

export default Layout;
