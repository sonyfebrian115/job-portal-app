import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Link } from "react-router-dom";

const CardDetail = () => {
  const [job, setJob] = useState(null);
  const { slug } = useParams();
  const url = `http://localhost:3001/jobs/${slug}`;

  useEffect(() => {
    axios
      .get(url)
      .then((response) => {
        setJob(response.data);
      })
      .catch((error) => {
        console.error("Error fetching job detail: ", error);
      });
  }, [url]);

  if (!job) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <main className=" bg-white px-6 md:px-16 py-6">
        <div className="flex flex-wrap justify-between ">
          <div className=" w-full md:w-8/12">
            <Link
              to="/"
              className="text-indigo-500 inline-flex items-center mt-4"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-4 h-4 mr-2"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18"
                />
              </svg>
              All jobs
            </Link>

            <div className=" mb-4">
              <span className="text-xs text-gray-500">
                Posted less than a minute ago
              </span>
              <h1 className="text-2xl">{job.title}</h1>
              <span className=" bg-sky-500 text-white p-1 text-xs mr-4">
                {job.employment_type}
              </span>
              <span className="text-xs">{job.location}</span>
              <span className="text-xs ml-4">{job.position_level}</span>
            </div>

            <div className=" mb-4">
              <h3 className="text-xl">Job Description</h3>
              <p className="mb-2">{job.responsibilities}</p>
            </div>
            <div className=" mb-4">
              <h3 className="text-xl">Minimum Qualification</h3>
              <p className="mb-2">{job.qualifications}</p>
            </div>
            <div className=" mb-4">
              <h3 className="text-xl">Minimum Experince</h3>
              <p className="mb-2">
                {job.minimum_experience ? job.minimum_experience : "-"}
              </p>
            </div>
            <div className=" mb-4">
              <h3 className="text-xl">Skill</h3>
              <p className="mb-2">{job.skill ? job.skill : "-"}</p>
            </div>
            <div className=" mb-4">
              <h3 className="text-xl">Benefit</h3>
              <p className="mb-2">{job.benefits ? job.benefits : "-"}</p>
            </div>
            <a
              href="#"
              className="bg-sky-500 hover:bg-sky-800 text-white text-center block rounded-full py-2"
            >
              Apply for this job
            </a>
          </div>

          <div className="w-full hidden md:block md:w-3/12">
            <div className="flex items-center h-screen w-full justify-center">
              <div className="max-w-xs">
                <div className="bg-white shadow-xl rounded-lg py-3">
                  <div className="photo-wrapper p-24">
                    <img
                      className="w-32 h-32 rounded-full mx-auto "
                      src="https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png"
                      alt=""
                    />
                  </div>
                  <div className="p-2">
                    <h3 className="text-center text-xl text-gray-900 font-medium leading-8">
                      John Doe
                    </h3>
                    <div className="text-center text-gray-400 text-xs font-semibold">
                      <p>Web Developer</p>
                    </div>

                    <div className="text-center my-3">
                      <a
                        href="#"
                        className="bg-sky-500  text-white text-center block rounded-full py-2 mb-4"
                      >
                        Apply for this job
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default CardDetail;
