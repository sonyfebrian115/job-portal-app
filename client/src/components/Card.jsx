import { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const Card = () => {
  const [jobs, setJobs] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:3001/jobs")
      .then((response) => {
        setJobs(response.data);
      })
      .catch((error) => {
        console.error("Error fetching jobs: ", error);
      });
  }, []);

  return (
    <>
      <section>
        <div className="container px-5 py-5 mx-auto">
          <div className="flex flex-wrap -m-4">
            {jobs.map((job, i) => (
              <div className="p-4 md:w-1/3" key={i}>
                <div className="h-full border-2 border-gray-200 border-opacity-60 drop-shadow-lg bg-white rounded-lg p-6">
                  <h1 className="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-900">
                    {job.title}
                  </h1>

                  <p className="leading-relaxed text-base mb-4">
                    {job.description}
                  </p>
                  <div className="flex mt-3">
                    <Link to={`/jobs/${job.id}`}>
                      <span className=" font-bold text-lg text-blue-800">
                        View Details
                      </span>
                    </Link>
                    <button className="flex ml-auto text-white bg-sky-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded">
                      Apply
                    </button>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
    </>
  );
};

Card.propTypes = {
  onDataReceived: PropTypes.func.isRequired,
};
export default Card;
