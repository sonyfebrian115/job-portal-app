// import Card from "./components/Card";
import CardDetail from "./components/CardDetails";
import Layout from "./pages/Layout";
import { Route, Routes } from "react-router-dom";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Layout />} />
        <Route path="/jobs/:slug" element={<CardDetail />} />
      </Routes>
    </>
  );
}

export default App;
